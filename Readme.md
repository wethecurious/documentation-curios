# CuriOs

## Introduction

CuriOs is our developer toolkit for screen based exhibits and experiences in We The Curious. If you're looking to develop an experience for We The Curious this is the place to start

CuriOs brings together years of experience in developing exhibits and online experiences to provide a platform which handles the tricky but vital sides of building an exhibit like screen accessibility, internationalization, logging and monitoring, deployment, show control integration - allowing you freedom to focus on the user experience.

In addition CuriOs also provides you access to advanced services that can be used to easily and safely build complex networked and data sharing experiences. This includes access to our user account and consent systems including our Physical Experience Token system which allows the user to easily link data to their account through RFID tags, device beacons and other technologies.

The platform itself is made up of various APIs and an front end toolkit which are currently being built and are being made open source as the reach beta stage.

## This Document

This document provides links to overviews of the component parts of CuriOs. More detailed documentation is available in the repos themselves.

Current docs are in the master brach of this repo. Stretch docs are in the develop branch.

## Platform Components

### Front End Toolkit

[Front End Toolkit/Readme.md](./Front%20End%20Toolkit/Readme.md)