# Front end toolkit

This is a toolkit which provides the component parts for building a screen based experience for We The Curious to be deployed on the floor or online.

- Component Library

Basic user interface components which are user tested, accessible, themeable and conform to our design language

- Build system

System to build for our various deployment targets, with linting to check code conforms to our style

- Layout templates

Basic starting layouts for common interface types

- Server template

A production server for serving the experience interface and certificates as well as providing back end integration with our systems

## Technology

Our components are built on React and Node in Typescript. You may use JavaScript or TypeScript when developing your exhibit.

We use redux for state management

Our primary deployment target is the web, so even in house experiences are served from the cloud.

Other deployment targets are Electron for local experiences or React Native

## Component library

The component library consists of basic user interface components which are injected with an externally provided theme from our Asset Service. This means the color palette, base sizing, icon set and animation speeds are fully customizable.

### Components

#### Layout

Layout components do not render visible but are used for structure

- Box

FlexBox based component for positioning elements

- Sidebar

Standard sidebar for control panels and browsers

- Masonry

Masonry style layout for image galleries

- Columns

Columns primarily for text formatting

- List

For rendering lists

- Modal

For pop-over modals

#### Presentation

Presentational components are controls and info which render on the screen. They rely on theme injection to appear correctly

[Previews are available here of the components rendered in the default theme](https://xd.adobe.com/spec/6486e54a-ed1b-4011-4f65-d118734db7df-12b7)

**Password**: Curi0us_D33ds

- Button

- Calendar

- Card

- Dropdown

- Header

- Heading

- Hero

- Icon

- IconNavBar

- IconSidebar

- InputAutoComplete

- InputText

- InputSearch

- Notification

- Markdown

- Radio

- Slider

- Tab

- Table

- Text

- Title

- Toggle

- Value

- Video Player

#### Icons

This is a list of icons available to the **Icon** component

```javascript
  AddOutline
  Add
  Collapse
  Delete,
  DeleteOutline,
  Expand,
  Remove,
  RemoveOutline,
  Share,
  Close,
  Down,
  GroupOutline,
  Group,
  Left,
  LevelBack,
  LevelUp,
  List,
  Menu,
  Next,
  Previous,
  Right,
  Search,
  Critical,
  CriticalOutline,
  ErrorOutline,
  Disabled,
  Ok,
  OkOutline,
  Unknown,
  UnknownOutline,
  Warning,
  WarningOutline,
```

#### Theme Colors

Theme are the colors available to themes with their default values. These can be customized in agreement with the look and feel of the exhibition or space

```javascript
{
    primary: '#209A8D',
    primaryInverse: '#28AEAF',
    secondary: '#28AEAF',
    secondaryInverse: '#51D5D6',
    appbarPrimary: '#323237',
    appbarPrimaryInverse: '#14141E',
    backgroundApp:'#F0F0FA',
    backgroundAppInverse:'#323237',
    backgroundComponent:'#FFFFFF',
    backgroundComponentInverse:'#505055',
    icon: '#209A8D',
    iconInverse: '#28AEAF',
    line: '#323237',
    lineInverse: '#F0F0FA',
    statusCritical: '#e0322a',
    statusCriticalInverse: '#e0322a',
    statusError:'#E0322A',
    statusErrorInverse: '#E0322A',
    statusWarning: '#EA7424',
    statusWarningInverse: '#EA7424',
    statusOk: '#209A8D',
    statusOkInverse: '#28AEAF',
    statusUnknown: '#AAAAAA',
    statusUnknownInverse: '#787878',
    statusDisabled: '#AAAAAA',
    statusDisabledInverse: '#787878',
    textPrimary: '#323232',
    textPrimaryInverse: '#FFFFFF',
    textSecondary: '#6E6E6E',
    textSecondaryInverse: '#A5A5A5',
    textAccent: '#209A8D',
    textAccentInverse: '#28AEAF',
    textH1: '#323232',
    textH1Inverse: '#58504a',
    textH2: '#323232',
    textH2Inverse: '#58504a',
    textH3: '#323232',
    textH3Inverse: '#58504a',
    textH4: '#323232',
    textH4Inverse: '#58504a',
    textH5: '#323232',
    textH5Inverse: '#58504a',
    textH6: '#323232',
    textH6Inverse: '#58504a',
    brandOneLow: '#E33A67',
    brandOneLowInverse: '#B5385D',
    brandOneMedium: '#C23582',
    brandOneMediumInverse: '#C23582',
    brandOneHigh: '#B5385D',
    brandOneHighInverse: '#E33A67',
    brandTwoLow:'#FFD840',
    brandTwoLowInverse:'#F9B002',
    brandTwoMedium: '#F9B002',
    brandTwoMediumInverse: '#F9B002',
    brandTwoHigh: '#F9B002',
    brandTwoHighInverse: '#FFD840',
    brandThreeLow:'#A1BF24',
    brandThreeLowInverse:'#3B9E3A',
    brandThreeMedium: '#7AB131',
    brandThreeMediumInverse:'#A1BF24',
    brandThreeHigh: '#3B9E3A',
    brandThreeHighInverse:'#A1BF24',
    brandFourLow:'#28B6DF',
    brandFourLowInverse:'#14549A',
    brandFourMedium: '#008ED0',
    brandFourMediumInverse:'#28B6DF',
    brandFourHigh: '#14549A',
    brandFourHighInverse: '#28B6DF',
    brandFiveLow: '#5D5395',
    brandFiveLowInverse: '#4F386E',
    brandFiveMedium: '#5D5395',
    brandFiveMediumInverse: '#5D5395',
    brandFiveHigh: '#4F386E',
    brandFiveHighInverse: '#5D5395',
    logoOne: '',
    logoOneInverse: '',
    logoTwo: '',
    logoTwoInverse: '',
    logoThree: '',
    logoThreeInverse: '',
    logoFour: '',
    logoFourInverse: '',
    logoFive: '',
    logoFiveInverse: '',
    logoSix: '',
    logoSixInverse: '',
    logoMono: '',
    logoMonoInverse: '',
}
```